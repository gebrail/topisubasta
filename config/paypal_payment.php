<?php

return array(
	# Account credentials from developer portal
	'Account' => array(
		'ClientId' => env('PAYPAL_CLIENT_ID', 'AddXTYAyyVEJTQvi1dlCnwzol_RRcdLYkdABnEevW18dDsZqj2PY3StFgEAbwZjE13oZKW1eR8F16BxD'),
		'ClientSecret' => env('PAYPAL_CLIENT_SECRET', 'EKM7hUqxrQ-sgSrgCIHKrcg8zP55d2yKxc1_I8bE_E_RKXvFMhPJIg1wo467fSqnGbW6IFn0E_R2Te2r'),
	
	),

	# Connection Information
	'Http' => array(
		// 'ConnectionTimeOut' => 30,
		'Retry' => 1,
		//'Proxy' => 'http://[username:password]@hostname[:port][/path]',
	),

	# Service Configuration
	'Service' => array(
		# For integrating with the live endpoint,
		# change the URL to https://api.paypal.com!
	'EndPoint' => 'https://api.sandbox.paypal.com',
	),


	# Logging Information
	'Log' => array(
		//'LogEnabled' => true,

		# When using a relative path, the log file is created
		# relative to the .php file that is the entry point
		# for this request. You can also provide an absolute
		# path here
		'FileName' => '../PayPal.log',

		# Logging level can be one of FINE, INFO, WARN or ERROR
		# Logging is most verbose in the 'FINE' level and
		# decreases as you proceed towards ERROR
		//'LogLevel' => 'FINE',
	),
);
