        
'use strict';

         var myApp = angular.module('myApp', []);
         
        myApp.service('multipartForm', ['$http', function($http){
   this.post = function(uploadUrl, data){
      var fd = new FormData();
      for(var key in data)
         fd.append(key, data[key]);
      $http.post(uploadUrl, fd, {
         transformRequest: angular.indentity,
         headers: { 'Content-Type': undefined }
      });
   }
}]);
        myApp.directive('fileModel', ['$parse', function($parse){
   return {
      restrict: 'A',
      link: function(scope, element, attrs){
         var model = $parse(attrs.fileModel);
         var modelSetter = model.assign;

         element.bind('change', function(){
            scope.$apply(function(){
               modelSetter(scope, element[0].files[0]);
            })
         })
      }
   }
}]);

    
    myApp.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});     

   myApp.controller('submitController', function($scope,$http,$window){
   $scope.customer = {};
   $scope.subastas={};
   $scope.subastas2={};
   $scope.producto={};
   $scope.activos = {};
   $scope.fotos={};
   $scope.persona={};
  $scope.currentPage = 0;
  $scope.pageSize = 1;


        $http.get('/productos')
        .success(function(data) {
       
            $scope.producto=data.su;
 
                
            })
        .error(function(data) {
            console.log('Error: ' + data);
        });

   
    $scope.listartodo = function(opcion) {

      $http.post('/listar', {opcion:opcion}).
  success(function(data, status, headers, config) {
    
    if(data.su.length!=0){

    $scope.subastas=data.su;
  
  }else{
    alert("No se encuentran subastas");

  }

    
    // this callback will be called asynchronously
    // when the response is available
  }).
  error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
       
      }

 $scope.numberOfPages=function(){
        return Math.ceil($scope.subastas.length/$scope.pageSize);                
    }

 
$scope.imagen = function(subasta) {

    $http.post('/imagen', {id:subasta.foto}).
  success(function(data, status, headers, config) {
    
    if(data.su.length!=0){

    $scope.fotos=data.su;
  
  }else{
    alert("No se encuentran fotos");

  }

    
    // this callback will be called asynchronously
    // when the response is available
  }).
  error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });


     }



$scope.aceptar = function(subasta) {

       
      $http.post('/aceptar', {id:subasta.id}).
  success(function(data, status, headers, config) {
          alert(data);

    
    // this callback will be called asynchronously
    // when the response is available
  }).
  error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
       
      }


      $scope.agregar = function(product,activo) {
       
      $http.post('/agregar', {id:product.id,precio:activo.valor}).
  success(function(data, status, headers, config) {
          
          
        $http.get('/productos')
        .success(function(dataa) {
       
            $scope.producto=dataa.su;
 

                
            })
        .error(function(data) {
            console.log('Error: ' + data);
        });

    
    // this callback will be called asynchronously
    // when the response is available
  }).
  error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
       
      }




      $scope.rechazar = function(subasta) {

       
      $http.post('/rechazar', {id:subasta.id}).
  success(function(data, status, headers, config) {
          alert(data);
          $scope.subastas=data;

    
    // this callback will be called asynchronously
    // when the response is available
  }).
  error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
       
      }

     


       
      $scope.cambiar=function(subasta){
        $scope.subastas2=subasta;
      }

      $scope.ofertar=function(oferta){


          $http.post('/ofertar', {'valor' : oferta.valor_inicial ,'subasta' :oferta.id ,'producto' : oferta.id_producto}).
  success(function(data, status, headers, config) {
    alert(data);
    /*   */
        $http.post('/listar', {opcion:1}).
  success(function(data2, status, headers, config) {
    $scope.subastas=data2.su;
  $http.post('/getofertar', {id:oferta.id}).
  success(function(data3, status, headers, config) {
for(var x in data3.su){
            $scope.subastas2=data3.su[x]; 
          }
    
    // this callback will be called asynchronously
    // when the response is available
  }).
  error(function(data1, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
    
    // this callback will be called asynchronously
    // when the response is available
  }).
  error(function(data2, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });

    // this callback will be called asynchronously
    // when the response is available
  }).
  error(function(data3, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
     
        
       
      }


  $scope.ingresar = function(user) {



         $http.post('/registrar', {nombre:user.nombre,contra:user.contra}).
  success(function(data, status, headers, config) {
  if(!data){alert("vacio");}else{
            alert(data);
            var $r=0;
         for(var i in data.users) {
                alert(data.users[i].id);

            if(data.users[i].correo!=null){

    $window.location.href = '/perfil';
    $r=1;
            }
            

                  }
                  if($r!=1){
                    alert("Usuario no encontrado");
                
                $scope.activos.error="error";
            }


        }
    
    // this callback will be called asynchronously
    // when the response is available
  }).
  error(function(data, status, headers, config) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });
       

    };


});


			
