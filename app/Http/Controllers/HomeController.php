<?php

namespace App\Http\Controllers; //Add this line

use Illuminate\Http\Request; //Add this line
use App\Http\Requests; //Add this line
use App\Http\Controllers\Controller; //Add this line

class HomeController extends Controller {

    public function welcome(){ 
       return 'I use the controller.'; 
    }

}