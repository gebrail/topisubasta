<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subasta extends Model
{
	protected $table = 'subasta';

	public $timestamps = false;

	public function scopeagregar($query,$su,$producto,$t,$date,$nuevafecha,$id){
	$subasta = new subasta;
	$subasta->id=$su;
	$subasta->id_producto=$producto;
	$subasta->valor_inicial=$t;
	$subasta->valor_incrementar=50;
	$subasta->fecha_inicial=$date;
	$subasta->fecha_final=$nuevafecha;
	$subasta->estado=1;
	$subasta->propietario=$id;
	$subasta->aceptado=0;
	$subasta->save();
	}
	public function scopebuscar($query,$id){
		return $query->find($id);


	}
	public function scopetraerpersona(){
		 $this->hasOne('App\persona','correo');
		 
		 return $this;
	}
    //



}
