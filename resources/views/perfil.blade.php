<!DOCTYPE html>
<html lang="en" ng-app="myApp">
<head>
<meta charset="utf-8">
<title>Topi-Subastas</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="Bootstrap 3 template for corporate business" />


<!-- css -->
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/cubeportfolio.min.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />
<link href="css/thumbnail-gallery.css" rel="stylesheet">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">



<!-- Theme skin -->
<link id="t-colors" href="skins/default.css" rel="stylesheet" />


	<!-- boxed bg -->
	<link id="bodybg" href="bodybg/bg1.css" rel="stylesheet" type="text/css" />

<!-- =======================================================
    Theme Name: Sailor
    Theme URL: https://bootstrapmade.com/sailor-free-bootstrap-theme/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
======================================================= -->
<style>
body {font-family: "Lato", sans-serif;}

/* Style the tab */
div.tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
div.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of buttons on hover */
div.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
div.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
    height: auto;
    min-height: 300px;

}

/* Style the close button */
.topright {
    float: right;
    cursor: pointer;
    font-size: 20px;
}

.topright:hover {color: red;}
</style>
</head>
<body  ng-controller="submitController" >



<div id="wrapper" >

	<!-- start header -->
	<header>
			<div class="top">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<ul class="topleft-info">
								<li><i class="fa fa-phone"></i> +62 088 999 123</li>
							</ul>
						</div>
						<div class="col-md-6">
						<div id="sb-search" class="sb-search">
							<form>
								<input class="sb-search-input" placeholder="Enter your search term..." type="text" value="" name="search" id="search">
								<input class="sb-search-submit" type="submit" value="">
								<span class="sb-icon-search" title="Click to start searching"></span>
							</form>
						</div>


						</div>
					</div>
				</div>
			</div>	
			
        <div class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img src="img/logo.png" alt="" width="199" height="52" /></a>
                </div>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
							<a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Home <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="index.html">Home slider 1</a></li>
                                <li><a href="index2.html">Home slider 2</a></li>
								
                            </ul>				
						
						</li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Features <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="typography.html">Typography</a></li>
                                <li><a href="components.html">Components</a></li>
								<li><a href="pricing-box.html">Pricing box</a></li>
								<li class="dropdown-submenu">
									<a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown">Pages</a>
									<ul class="dropdown-menu">
										<li><a href="fullwidth.html">Full width</a></li>
										<li><a href="right-sidebar.html">Right sidebar</a></li>
										<li><a href="left-sidebar.html">Left sidebar</a></li>
										<li><a href="comingsoon.html">Coming soon</a></li>
										<li><a href="search-result.html">Search result</a></li>
										<li><a href="404.html">404</a></li>
										<li><a href="register.html">Register</a></li>
										<li><a href="login.html">Login</a></li>
									</ul>	
								</li>
                            </ul>
                        </li>
                        <li class="active"><a href="portfolio.html">Portfolio</a></li>
                        <li class="dropdown"><a href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">Blog <i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
                                <li><a href="blog-rightsidebar.html">Blog right sidebar</a></li>
                                <li><a href="blog-leftsidebar.html">Blog left sidebar</a></li>
								<li><a href="post-rightsidebar.html">Post right sidebar</a></li>
								<li><a href="post-leftsidebar.html">Post left sidebar</a></li>
                            </ul>
						</li>
                        <li><a href="/salir">Salir</a></li>
                    </ul>
                </div>
            </div>
        </div>
	</header>
	<!-- end header -->
	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<ul class="breadcrumb">
					<li><a href="#"><i class="fa fa-home"></i></a><i class="icon-angle-right"></i></li>
					<li class="active">	{{$id}}</li>
				</ul>
			</div>
		</div>
	</div>
	</section>
	<section id="content">

	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h4 class="heading">Recent Works</h4>
			<!--	<div id="filters-container" class="cbp-l-filters-button">
					<div data-filter="todo" class="cbp-filter-item-active cbp-filter-item" ng-click="listartodo();">All<div class="cbp-filter-counter"></div></div>
					<div data-filter=".identity" class="cbp-filter-item">Identity<div class="cbp-filter-counter"></div></div>
					<div data-filter=".web-design" class="cbp-filter-item">Web Design<div class="cbp-filter-counter"></div></div>
					<div data-filter=".graphic" class="cbp-filter-item">Graphic<div class="cbp-filter-counter"></div></div>
					<div data-filter=".logo" class="cbp-filter-item">Logo<div class="cbp-filter-counter"></div></div>
				</div>
-->

				<div class="tab">
  <button class="tablinks" onclick="openCity(event, 'London')" ng-click="listartodo(1);">London</button>
  <button class="tablinks" onclick="openCity(event, 'Paris')" ng-click="listartodo(2);">Paris</button>
  <button class="tablinks" onclick="openCity(event, 'Tokyo')">Tokyo</button>
</div>
<div id="London" class="tabcontent">
 	<div ng-repeat="subasta in subastas" class="col-lg-3 col-md-4 col-xs-6 thumb" >
      <a class="thumbnail">
                    <img class="img-responsive" src="imagenes/@{{subasta.foto}}">
               @{{subasta.id}} </a>
               <button onclick="document.getElementById('id01').style.display='block'" class="w3-button w3-black" ng-click="cambiar(subasta);">Open Modal</button>
    </div>
</div>

<div id="Paris" class="tabcontent">
  <div ng-repeat="subasta in subastas" class="col-lg-3 col-md-4 col-xs-6 thumb" >
      <a class="thumbnail">
                    <img class="img-responsive" src="imagenes/@{{subasta.foto}}">
               @{{subasta.id}} </a>
               @{{subasta.valor_inicial}}
               <button ng-click="aceptar(subasta);">Aceptar</button>
               <button ng-click="rechazar(subasta);">Rechazar</button>
    </div>
</div>

<div id="Tokyo" class="tabcontent">
  
</div>

			

			
				
				<div class="cbp-l-loadMore-button">
					<a href="ajax/loadMore.html" class="cbp-l-loadMore-button-link">LOAD MORE</a>
</div>

			</div>

		</div>
	</div>

	</section>

	<footer>
	<div class="container">
		<div class="row">

			<div class="col-lg-12">
				<div class="widget">
					<h4>Agregar subasta</h4>
				<!--	<form action="{{ url('/agregar') }}" enctype="multipart/form-data" method="POST" id="form">
			{{ csrf_field() }} -->
			<div class="row">

				<div class="col-md-12">
					<!--<input type="file" name="image" /> 
					<input type="text" name="precio" /> -->
					   <input type="text" name="precio" required placeholder="Precio inicial" ng-model="activo.valor" /> 

		<div ng-repeat="product in producto" class="col-lg-3 col-md-4 col-xs-6 thumb">

                 <a class="thumbnail">
                    <img class="img-responsive" src="imagenes/@{{product.foto}}" value="@{{product.id}}" ng-model="product.id" ng-click="agregar(product,activo)">
               @{{product.id}} </a>
				</div>
				<div class="col-md-12">

					<button  class="btn btn-success">Crear subasta</button>
				</div>
			</div>
	

         </div> 
       <!--  </form>  -->
                    
				</div>
				</div>




		</div>
	</div>
<div id="id01" class="w3-modal">
    <div class="w3-modal-content">
      <header class="w3-container w3-teal"> 
        <span onclick="document.getElementById('id01').style.display='none'" 
        class="w3-button w3-display-topright">&times;</span>
        <h2>Realizar oferta</h2>
      </header>
      <div class="w3-container">
        <p>Id de las subasta:@{{subastas2.id}}</p>
        <p>Mayor precio ofertado @{{subastas2.valor_incrementar}} </p>
        <p>valor a ofertar @{{subastas2.valor_inicial}}</p>
      <button ng-click='ofertar(subastas2);'>Ofertar</button>
      </div>
      <footer class="w3-container w3-teal">
        <p></p>
      </footer>
    </div>
  </div>

	<div id="sub-footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="copyright">
						<p>&copy; Sailor Theme - All Right Reserved</p>
                        <div class="credits">
                            <!-- 
                                All the links in the footer should remain intact. 
                                You can delete the links only if you purchased the pro version.
                                Licensing information: https://bootstrapmade.com/license/
                                Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Sailor
                            -->
                            <a href="https://bootstrapmade.com/free-business-bootstrap-themes-website-templates/">Business Bootstrap Themes</a> by <a href="https://bootstrapmade.com/">BootstrapMade</a>
                        </div>
					</div>
				</div>
				<div class="col-lg-6">
					<ul class="social-network">
						<li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
						<li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	</footer>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/stellar.js"></script>
<script src="js/classie.js"></script>
<script src="js/uisearch.js"></script>
<script src="js/jquery.cubeportfolio.min.js"></script>
<script src="js/google-code-prettify/prettify.js"></script>
<script src="js/animate.js"></script>
<script src="js/custom.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.3/angular-route.js"></script>
<script src="js/script2.js"></script>
<script src="js/dropzone.js"></script>
<script>
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>


</body>
</html>