<!DOCTYPE html>
<html lang="en" ng-app="myApp">
<head>
<meta charset="utf-8">
<title>Topi-Subastas</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="Bootstrap 3 template for corporate business" />
<!-- css -->
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/cubeportfolio.min.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />
<link href="css/w3.css" rel="stylesheet">



<!-- Theme skin -->
<link id="t-colors" href="skins/default.css" rel="stylesheet" />


	<!-- boxed bg -->
	<link id="bodybg" href="bodybg/bg1.css" rel="stylesheet" type="text/css" />


<!-- =======================================================
    Theme Name: Sailor
    Theme URL: https://bootstrapmade.com/sailor-free-bootstrap-theme/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
======================================================= -->

</head>
<body ng-controller="submitController" data-ng-init="listartodo(1);" >



<div id="wrapper">

	<!-- start header -->
		<header>
			<div class="top">
				<div class="container">
					
				</div>
			</div>	
			
        <div class="navbar navbar-default navbar-top" style = "background-color:#DA0404">
            <div class="container" >
                <div class="navbar-header" >
                    <button   type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img src="img/LOGO_TOPICOS.png" alt="" width="76" height="50" /></a>
                </div>
                <div class="navbar-collapse collapse"  >
                    <ul class="nav navbar-nav">
                        <li ><a Style = "color: white" href="/">INICIO</a></li>
                        <li class="dropdown">
                            <a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">PERFIL  <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="/perfil">VER PERFIL</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">TU SUBASTA  <i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
                                <li><a href="/registrarsubasta">CREA TU SUBASTA</a></li>
                                <li><a href="/terminadas">DECIDE EL DESTINO DE TU CASA</a></li>
								
                            </ul>
						</li>
                        <li class="dropdown"><a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">SUBASTAR<i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
                                <li><a href="/subasta">PARTICIPA EN UNA SUBASTA</a></li>
                                
                            </ul>
						</li>
						<li class="dropdown"><a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">TRANSACCIONES  <i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
                                <li><a href="/gettransaccion">INICIAR UNA TRANSACCIÓN</a></li>
                                
                            </ul>
						</li>
                        <li><a Style = "color: white" href="/salir">Salir</a></li>
                    </ul>
                </div>
            </div>
        </div>
	</header>
	<!-- end header -->

	<section id="content">
	<div class="container">
		
			
		<div  Style= "border: 1px solid; width:1200px;" class="col-md-5" ng-repeat="subasta in subastas  | startFrom:currentPage*pageSize | limitTo:pageSize" >
			<br /> 
					<div class="col-md-6">
				
							<img src="imagenes/{{subasta.foto}}" alt="" class="img-responsive pull-left ig" ng-click="imagen(subasta);" onclick="document.getElementById('id02').style.display='block'"/>
							<br>
							<h5>Nombre: </h5>
							<input  class= "form-control input-lg" type="text" placeholder= "Nombre" value="{{subasta.nombre}}"/>	<br />
							<h5>Descripcion: </h5>
				<textarea  class= "form-control input-lg" id="" cols="10" rows="5" placeholder= " Descripcion" /> {{subasta.descirpcion}} </textarea><br />
					</div> 
					<div class="col-md-6">
				
				<h5>Ubicación: </h5>
				<input class= "form-control input-lg" type="text" placeholder= "Ubicación" value="{{subasta.ubicacion}}" />	<br />
				<h5>Valor inicial: </h5>
				<input class= "form-control input-lg" type="text" placeholder= "Valor inicial ($)" value="{{subasta.valor_inicial}}"/>	<br /> 
				<h5>Tiempo restante: </h5>
				<input id="final2" class= "form-control input-lg" type="text" placeholder= "Tiempo restante"/>	<br /> 
				<input id="final" type="hidden" value="{{subasta.fecha_final}}"/>	<br /> 
               <button onclick="document.getElementById('id01').style.display='block'" Style = "background-color:#DA0404 ; opacity:0.7" class="btn btn-primary btn-block btn-lg" ng-click="cambiar(subasta);">Ofertar</button> <br>

               <button onclick="tiempo(document.getElementById('final').value);" Style = "background-color:#DA0404 ; opacity:0.7" class="btn btn-primary btn-block btn-lg">tiempo</button>
					<br>
			</div>

		</div>


		
		  <button ng-disabled="currentPage == 0" ng-click="currentPage=currentPage-1" Style = "background-color:#DA0404 ; opacity:0.7 ; width:150px" value="" class="btn btn-primary btn-block btn-lg" onclick="limpiar()">
        Previous
    </button>
    {{currentPage+1}}/{{numberOfPages()}}
   <button ng-disabled="currentPage >= data.length/pageSize - 1" Style = "background-color:#DA0404 ; opacity:0.7 ; width:150px" ng-click="currentPage=currentPage+1" Style = "opacity:0.7 ; width:100px" value="" class="btn btn-primary btn-block btn-lg" onclick="limpiar()">
        Next </button>
		
				
		
		
		
			
			<div Style= "float:left" class="col-md-4">
			<br />
			
			<div Style= "float:right" class="col-md-4">
			</div>
			</div>
		</div>
	
	</section>

	<footer Style = "background-color : white">
	
	<div id="sub-footer">
		<div class="container">
			<div class="row">
				
					<div class="copyright">
						<p>&copy; Sailor Theme - All Right Reserved</p>
                        <div class="credits">
                            
                            <a href="https://bootstrapmade.com/free-business-bootstrap-themes-website-templates/">Business Bootstrap Themes</a> by <a href="https://bootstrapmade.com/">BootstrapMade</a>
						                     
					   </div>
						
					</div>
				 
				
			</div>
		</div>
	</div>
	</footer>
</div>
<div id="id01" 
   <button ng-disabled="currentPage >= data.length/pageSize - 1"  class="w3-modal">
    <div  class="w3-modal-content">
      <header  class="w3-container w3-teal" > 
        <span  onclick="document.getElementById('id01').style.display='none'" 
        class="w3-button w3-display-topright">&times;</span>
        <h2 > Realizar oferta</h2>
      </header>
      <div class="w3-container">
        <p>Id de las subasta:{{subastas2.id}}</p>
        <p>Mayor precio ofertado {{subastas2.valor_incrementar}} </p>
        <p>valor a ofertar {{subastas2.valor_inicial}}</p>
      <button ng-click='ofertar(subastas2);'>Ofertar</button>
      </div>
      <footer class="w3-container w3-teal">
        <p></p>
      </footer>
    </div>
  </div>
  <div id="id02" 
     <button ng-disabled="currentPage >= data.length/pageSize - 1"  class="w3-modal">
    <div class="w3-modal-content">
      <header class="w3-container w3-teal"> 
        <span onclick="document.getElementById('id02').style.display='none'" 
        class="w3-button w3-display-topright">&times;</span>
      </header>
      <div class="w3-container">
      <h1>Fotos</h1>
        <div ng-repeat="su in fotos">
        	<img src="imagenes/{{su.nombre_foto}}" alt="" class="img-responsive pull-left im"  />
        </div>
      </div>
      <footer class="w3-container w3-teal">
        <p></p>
      </footer>
    </div>
  </div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.3/angular-route.js"></script>
<script src="js/script2.js"></script>
<script src="js/dropzone.js"></script>
<script src="http://momentjs.com/downloads/moment.min.js"></script>

<script>
var aux;

function tiempo(fecha){

	aux=fecha;
	

var interval =setInterval(function(){ 
var actual=moment();
var final=moment(aux);
var año=final.diff(actual,"years");
var meses=final.diff(actual,"month");
var dias=final.diff(actual,"day");
var h=final.diff(actual,"hours");
var s=final.diff(actual,"seconds");
s/=60;
if(s<1){
	document.getElementById('final2').value="tiempo de subasta agotado";
}else{
document.getElementById('final2').value="años:"+año+"meses:"+meses+"dias:"+dias+"horas:"+h+"minutos/segundos:"+s;
}
 }, 1000);

}

function limpiar(){
alert("limpiar");
document.getElementById('final').value=null;
aux=null;
}




</script>
</body>
</html>
