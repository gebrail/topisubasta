<html ng-app='myApp'>  
<head>  
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" />
</head>  
<body>  
  <header>
    <h1>Angular Routing</h1>
      <nav> 
        <ul> 
          <li><a href="#">Inicio</a></li>
          <li><a href="#acerca">Acerca de</a></li>
          <li><a href="#contacto">Contacto</a></li>         </ul> 
      </nav>
    </header>
    <div id="main">
      <!-- Aquí inyectamos las vistas -->
      {{$entrada}}
    </div>
    </body>
  </html>