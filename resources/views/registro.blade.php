<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Topi-Subastas</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="Bootstrap 3 template for corporate business" />
<!-- css -->
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/cubeportfolio.min.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />

<!-- Theme skin -->
<link id="t-colors" href="skins/default.css" rel="stylesheet" />

	<!-- boxed bg -->
	<link id="bodybg" href="bodybg/bg1.css" rel="stylesheet" type="text/css" />

<!-- =======================================================
    Theme Name: Sailor
    Theme URL: https://bootstrapmade.com/sailor-free-bootstrap-theme/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
======================================================= -->

</head>
<body>


<div id="wrapper">
	<!-- start header -->
	<header>
			<div class="top">
				<div class="container">
					
				</div>
			</div>	
			
        <div class="navbar navbar-default navbar-static-top" style = "background-color:#DA0404">
            <div class="container" >
                <div class="navbar-header" >
                    <button   type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img src="img/LOGO_TOPICOS.png" alt="" width="76" height="50" /></a>
                </div>
                <div class="navbar-collapse collapse"  >
                    <ul class="nav navbar-nav">
                        <li ><a Style = "color: white" href="/">INICIO</a></li>
                        <li class="dropdown">
                            <a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">PERFIL  <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="/perfil">VER PERFIL</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">TU SUBASTA  <i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
                                <li><a href="/registrarsubasta">CREA TU SUBASTA</a></li>
                                <li><a href="/terminadas">DECIDE EL DESTINO DE TU CASA</a></li>
								
                            </ul>
						</li>
                        <li class="dropdown"><a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">SUBASTAR<i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
                                <li><a href="/subasta">PARTICIPA EN UNA SUBASTA</a></li>
                                
                            </ul>
						</li>
						<li class="dropdown"><a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">TRANSACCIONES  <i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
                                <li><a href="/gettransaccion">INICIAR UNA TRANSACCIÓN</a></li>
                                
                            </ul>
						</li>
                        <li><a Style = "color: white" href="/salir">Salir</a></li>
                    </ul>
                </div>
            </div>
        </div>
	</header>
	<!-- end header -->
	<section id="inner-headline">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				
			</div>
		</div>
	</div>
	</section>
	<section id="content">
<div class="container">

<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
		<form role="form" class="register-form" action="/registrarp" method="post" enctype="multipart/form-data"> 		
		    {{ csrf_field() }}

			<h2 Style = "color:#DA0404"><img  src="img/LOGO_TOPICOS.png" alt="" width="80" height="80" border="0" /> Regístrate<small Style = "color:#DA0404"> Es gratis y siempre lo será.</small></h2>
			<hr class="colorgraph">
			<div class="form-group">
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
                        <input type="text" name="nombres" id="first_name" class="form-control input-lg" placeholder="Nombre(s)" tabindex="1" required>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="text" name="apellidos" id="last_name" class="form-control input-lg" placeholder="Apellidos" tabindex="2" required>
					</div>
				</div>
			</div>
			<div class="form-group">
				<input type="text" name="apodo" id="display_name" class="form-control input-lg" placeholder="Apodo" tabindex="3" required>
			</div>
			<div class="form-group">
				<input type="number" name="telefono" id="display_name" class="form-control input-lg" placeholder="Teléfono" tabindex="3" required>
			</div>
			<div class="form-group">
			<input type="text" name="nacionalidad" id="display_name" class="form-control input-lg" placeholder="Nacionalidad" tabindex="1" required>
			</div>
			<div class="form-group">
			<input type="date" name="fecha_nac" id="display_name" class="form-control input-lg" placeholder="Fecha de nacimiento" tabindex="1" required>
			</div>
			
			<div class="form-group">
				<input type="email" name="email" id="email" class="form-control input-lg" placeholder="Email" tabindex="4" required>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="password" name="contrasena" id="password" class="form-control input-lg" placeholder="Contraseña" tabindex="5" required>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="password" name="confircontra" id="password_confirmation" class="form-control input-lg" placeholder="Confirme su contraseña" tabindex="6" required>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-4 col-sm-3 col-md-3">
					<span class="button-checkbox">
						<input type="checkbox" name="acepto" value="1" required>Acepto
                        <input type="checkbox" name="t_and_c" id="t_and_c" class="hidden" value="1">
					</span>
				</div>
				<div class="col-xs-8 col-sm-9 col-md-9">
					 Al hacer click en <strong class="label label-primary">Registrarse</strong>, usted acepta los <a href="{{ asset('files/file.pdf') }}" target="_blank">Terminos y condiciones</a> establecidos por este sitio.
				</div>
			</div>
			
			<hr class="colorgraph">
			<div class="row">
				<div class="col-xs-12 col-md-6"><input Style = "background-color:#DA0404 ; opacity:0.7" type="submit" value="Registrarse" class="btn btn-theme btn-block btn-lg" tabindex="7"></div>
				<div class="col-xs-12 col-md-6">Ya tienes una cuenta? <a href="/login">Ingresa</a></div>
			</div>
		</form>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="t_and_c_m" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Terminos & Condiciones</h4>
			</div>
			<div class="modal-body">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Acepto</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</div>
	</section>

	<footer Style = "background-color : white">
	
	<div id="sub-footer">
		<div class="container">
			<div class="row">
				
					<div class="copyright">
						<p>&copy; Sailor Theme - All Right Reserved</p>
                        <div class="credits">
                            
                            <a href="https://bootstrapmade.com/free-business-bootstrap-themes-website-templates/">Business Bootstrap Themes</a> by <a href="https://bootstrapmade.com/">BootstrapMade</a>
						                     
					   </div>
						
					</div>
				 
				
			</div>
		</div>
	</div>
	</footer>
	
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/stellar.js"></script>
<script src="js/classie.js"></script>
<script src="js/uisearch.js"></script>
<script src="js/jquery.cubeportfolio.min.js"></script>
<script src="js/google-code-prettify/prettify.js"></script>
<script src="js/animate.js"></script>
<script src="js/custom.js"></script>

</body>
</html>