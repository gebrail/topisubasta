<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Sailor - Bootstrap 3 corporate template</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="Bootstrap 3 template for corporate business" />
<!-- css -->
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/cubeportfolio.min.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />

<!-- Theme skin -->
<link id="t-colors" href="skins/default.css" rel="stylesheet" />

	<!-- boxed bg -->
	<link id="bodybg" href="bodybg/bg1.css" rel="stylesheet" type="text/css" />
<!-- =======================================================
    Theme Name: Sailor
    Theme URL: https://bootstrapmade.com/sailor-free-bootstrap-theme/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
======================================================= -->
</head>
<body>


<div id="wrapper">
	<!-- start header -->
	<header>
			<div class="top">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<ul class="topleft-info">
								<li><i class="fa fa-dollar"></i> 1.000.000</li>
							</ul>
						</div>
						<div align= "right">
						
							<li><i class="fa fa-user"></i> Anderson Acuña</li>

						</div>
					</div>
				</div>
			</div>	
			
        <div class="navbar navbar-default navbar-static-top" style = "background-color:#DA0404">
            <div class="container" >
                <div class="navbar-header" >
                    <button   type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img src="img/LOGO_TOPICOS.png" alt="" width="76" height="50" /></a>
                </div>
                <div class="navbar-collapse collapse ">
                    <ul class="nav navbar-nav">
                        <li ><a Style = "color: white" href="index.html">INCIO</a></li>
                        <li class="dropdown">
                            <a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">PERFIL  <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="perfil.html">VER PERFIL</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">TU SUBASTA  <i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
                                <li><a href="crear_subasta.html">CREA TU SUBASTA</a></li>
                                <li><a href="destino_subasta.html">DECIDE EL DESTINO DE TU CASA</a></li>
								
                            </ul>
						</li>
                        <li class="dropdown"><a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">SUBASTAR<i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
                                <li><a href="blog-rightsidebar.html">PARTICIPA EN UNA SUBASTA</a></li>
                                
                            </ul>
						</li>
						<li class="dropdown"><a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">TRANSACCIONES  <i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
                                <li><a href="blog-rightsidebar.html">INICIAR UNA TRANSACCIÓN</a></li>
                                
                            </ul>
						</li>
                        <li><a Style = "color: white" href="contact.html">CONTACTO</a></li>
                    </ul>
                </div>
            </div>
        </div>
	</header>
	<!-- end header -->
	<section id="inner-headline">
	
	</section>
	<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-lg-4">
				<h2 align = "center"><i class= "fa fa-user"></i>Afourfour</h2> <br />
			<div class="form-group">
			<input value = "andersonfcblive@gmail.com" type="email" name="email" id="email" class="form-control input-lg" placeholder="Email" tabindex="4">
			</div>
			<div class="form-group">
			<input value= "314 377 9688" type="text" name="telefono" id="display_name" class="form-control input-lg" placeholder="Teléfono" tabindex="3">
			</div>
			<div class="form-group">
			<input value = "Colombia" type="text" name="nacionalidad" id="display_name" class="form-control input-lg" placeholder="Nacionalidad" tabindex="1">
			</div>
			<div class="form-group">
			<input  type="date" name="fecha_nac" id="display_name" class="form-control input-lg" placeholder="Fecha de nacimiento" tabindex="1">
			</div>
			
				<input Style = "background-color:#DA0404 ; opacity:0.7" type="submit" value="Edita tu perfil" class="btn btn-primary btn-block btn-lg" tabindex="7">
			</div>
			<div class="col-lg-4">
				<h3 align= "center">¿Quieres cambiar tu contraseña?</h3>
				<div class="form-group">
				<br />
						<input Style = "width : 260px" type="password" name="contrasenaantigua"  class="form-control input-lg" placeholder="Antigua contraseña" tabindex="5">
					</div>
					<div class="form-group">
						<input Style = "width : 260px" type="password" name="contrasenanueva" class="form-control input-lg" placeholder="Nueva contraseña" tabindex="5">
					</div>
					<div class="form-group">
						<input Style = "width : 260px" type="password" name="confircontrasenanueva" class="form-control input-lg" placeholder="Confirma nueva contraseña" tabindex="5">
					</div>
					<input Style = "background-color:#DA0404 ; opacity:0.7" type="submit" value="Cambia tu contraseña" class="btn btn-primary btn-block btn-lg" tabindex="7">
			</div>
			<div class="col-lg-4">
				<h3 align = "center">Productos ganados</h3>
				<img src="img/dummies/dummy-1.jpg" alt="" class="img-responsive pull-left" />
				
			</div>
			
		</div>
	
		
	</div>
	</section>

	<footer Style = "background-color : white">
	
	<div id="sub-footer">
		<div class="container">
			<div class="row">
				
					<div class="copyright">
						<p>&copy; Sailor Theme - All Right Reserved</p>
                        <div class="credits">
                            
                            <a href="https://bootstrapmade.com/free-business-bootstrap-themes-website-templates/">Business Bootstrap Themes</a> by <a href="https://bootstrapmade.com/">BootstrapMade</a>
						                     
					   </div>
						
					</div>
				 
				
			</div>
		</div>
	</div>
	</footer>
	
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.min.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/stellar.js"></script>
<script src="js/classie.js"></script>
<script src="js/uisearch.js"></script>
<script src="js/jquery.cubeportfolio.min.js"></script>
<script src="js/google-code-prettify/prettify.js"></script>
<script src="js/animate.js"></script>
<script src="js/custom.js"></script>

	
</body>
</html>