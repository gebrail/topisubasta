<!DOCTYPE html>
<html lang="en" ng-app="myApp">
<head>
<meta charset="utf-8">
<title>Topi-Subastas</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="Bootstrap 3 template for corporate business" />
<!-- css -->
<link href="css/bootstrap.min.css" rel="stylesheet" />
<link href="css/cubeportfolio.min.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />

<!-- Theme skin -->
<link id="t-colors" href="skins/default.css" rel="stylesheet" />

	<!-- boxed bg -->
	<link id="bodybg" href="bodybg/bg1.css" rel="stylesheet" type="text/css" />
<!-- =======================================================
    Theme Name: Sailor
    Theme URL: https://bootstrapmade.com/sailor-free-bootstrap-theme/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
======================================================= -->
</head>
<body ng-controller="submitController" data-ng-init="listartodo(2);">


<div id="wrapper">
	<!-- start header -->
	<header>
			<div class="top">
				<div class="container">
					
				</div>
			</div>	
			
        <div class="navbar navbar-default navbar-static-top" style = "background-color:#DA0404">
            <div class="container" >
                <div class="navbar-header" >
                    <button   type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img src="img/LOGO_TOPICOS.png" alt="" width="76" height="50" /></a>
                </div>
                <div class="navbar-collapse collapse"  >
                    <ul class="nav navbar-nav">
                        <li ><a Style = "color: white" href="/">INICIO</a></li>
                        <li class="dropdown">
                            <a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">PERFIL  <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="/perfil">VER PERFIL</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">TU SUBASTA  <i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
                                <li><a href="/registrarsubasta">CREA TU SUBASTA</a></li>
                                <li><a href="/terminadas">DECIDE EL DESTINO DE TU CASA</a></li>
								
                            </ul>
						</li>
                        <li class="dropdown"><a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">SUBASTAR<i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
                                <li><a href="/subasta">PARTICIPA EN UNA SUBASTA</a></li>
                                
                            </ul>
						</li>
						<li class="dropdown"><a Style = "color: white" href="#" class="dropdown-toggle " data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">TRANSACCIONES  <i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
                                <li><a href="/gettransaccion">INICIAR UNA TRANSACCIÓN</a></li>
                                
                            </ul>
						</li>
                        <li><a Style = "color: white" href="/salir">Salir</a></li>
                    </ul>
                </div>
            </div>
        </div>
	</header>
	<!-- end header -->
	<section id="content"  >
		<div class="container"  >
		<div  Style= "border: 1px solid ; float:right" class="col-md-5" ng-repeat="subasta in subastas">
				<br /> 
					<div class="col-md-6">
					
					<a>	<img src="imagenes/{{subasta.foto}}" alt="" class="img-responsive pull-left" /> {{subasta.id}}</a>
						<h3 align= "center"><label for=""><i class="fa fa-dollar"></i>{{subasta.valor_inicial}}</label></h3>
			
					</div> 
						
					<div class="col-md-6">
				
						<input  Style = "opacity:0.7" type="submit" value="Aceptar oferta" class="btn btn-primary btn-block btn-lg" tabindex="7" ng-click="aceptar(subasta);">
						<input Style = "background-color:#DA0404 ; opacity:0.7" type="submit" value="Rechazar oferta" class="btn btn-primary btn-block btn-lg" tabindex="7" ng-click="rechazar(subasta);">				

					</div>
			</div>	
		
		</div>
		
	</section>
	
	

	
	
	
	
	
	

	<footer Style = "background-color : white">
	
	<div id="sub-footer">
		<div class="container">
			<div class="row">
				
					<div class="copyright">
						<p>&copy; Sailor Theme - All Right Reserved</p>
                        <div class="credits">
                            
                            <a href="https://bootstrapmade.com/free-business-bootstrap-themes-website-templates/">Business Bootstrap Themes</a> by <a href="https://bootstrapmade.com/">BootstrapMade</a>
						                     
					   </div>
						
					</div>
				 
				
			</div>
		</div>
	</div>
	</footer>
	
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<!-- javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.2.3/angular-route.js"></script>
<script src="js/script2.js"></script>
<script src="js/dropzone.js"></script>
	
</body>
</html>